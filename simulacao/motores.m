clc;
clear all;
close all;

% parametros

J = 1;  % kg.m^2
b = 1;  % N.m.s/rad
K = 1;  % adimensional


% espaco de estados

A = [ -b/J   0 
       1     0 ];

B = [ K/J
      0   ];

C = [ 1  0 
      0  1 ];

D = [ 0
      0 ];

% y1 : velocidade (rad/s)
% y2 : angulo (rad)


pkg load control


% tempo continuo
      
sys = ss(A, B, C, D);

step(sys, 5);


% tempo discreto (exato)

sysd = c2d(sys, 0.1, 'zoh');

figure;

step(sysd, 5);


% tempo discreto (diferencas finitas)

% TODO
